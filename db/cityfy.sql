-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 24, 2016 at 10:39 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cityfy`
--

-- --------------------------------------------------------

--
-- Table structure for table `conversation`
--

CREATE TABLE `conversation` (
  `c_id` int(11) NOT NULL,
  `user_one` int(11) NOT NULL,
  `user_two` int(11) NOT NULL,
  `ip` varchar(30) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  `user_one_status` enum('0','1') NOT NULL DEFAULT '0',
  `user_two_status` enum('0','1') NOT NULL DEFAULT '0',
  `user_one_read` enum('0','1') NOT NULL DEFAULT '0',
  `user_two_read` enum('0','1') NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conversation`
--

INSERT INTO `conversation` (`c_id`, `user_one`, `user_two`, `ip`, `time`, `user_one_status`, `user_two_status`, `user_one_read`, `user_two_read`, `ts`) VALUES
(11, 33, 40, '::1', 1464075853, '0', '0', '0', '0', '2016-05-24 07:19:06'),
(12, 33, 41, '::1', 1464077688, '0', '0', '0', '0', '2016-05-24 08:14:48');

-- --------------------------------------------------------

--
-- Table structure for table `conversation_reply`
--

CREATE TABLE `conversation_reply` (
  `cr_id` int(11) NOT NULL,
  `reply` text,
  `user_id_fk` int(11) NOT NULL,
  `ip` varchar(30) NOT NULL,
  `time` int(11) NOT NULL,
  `c_id_fk` int(11) NOT NULL,
  `user_id_fk_2` int(10) NOT NULL,
  `sender_d_status` enum('0','1') NOT NULL DEFAULT '0',
  `receiver_d_status` enum('0','1') NOT NULL DEFAULT '0',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `conversation_reply`
--

INSERT INTO `conversation_reply` (`cr_id`, `reply`, `user_id_fk`, `ip`, `time`, `c_id_fk`, `user_id_fk_2`, `sender_d_status`, `receiver_d_status`, `ts`) VALUES
(1, 'hi', 27, '197.232.15.156', 1446478452, 1, 32, '1', '0', '2015-11-02 15:34:12'),
(2, 'hey', 27, '197.232.15.156', 1446478549, 1, 32, '0', '0', '2015-11-02 15:35:49'),
(3, 'What kinds of food do you deal with?', 19, '197.232.15.156', 1446478999, 2, 27, '0', '0', '2015-11-02 15:43:19'),
(4, 'I need an answer!!!', 19, '197.232.15.156', 1446479131, 2, 27, '0', '0', '2015-11-02 15:45:31'),
(5, 'What kinds of events do you deal with?', 19, '197.232.15.156', 1446479200, 3, 26, '0', '0', '2015-11-02 15:46:40'),
(6, 'I need an answer asap!!', 19, '197.232.15.156', 1446479242, 3, 26, '0', '0', '2015-11-02 15:47:22'),
(7, 'I need an answer asap!!', 19, '197.232.15.156', 1446479242, 3, 26, '1', '0', '2015-11-02 15:47:22'),
(8, 'hey', 19, '197.232.15.156', 1446479265, 3, 26, '0', '0', '2015-11-02 15:47:45'),
(9, 'Hello', 27, '105.56.216.127', 1446484023, 4, 31, '0', '0', '2015-11-02 17:07:03'),
(10, 'we specialize in all types of delicacies', 27, '105.56.216.127', 1446484145, 2, 19, '0', '0', '2015-11-02 17:09:05'),
(11, 'Hello', 31, '105.56.216.127', 1446485796, 5, 17, '0', '0', '2015-11-02 17:36:36'),
(13, 'waiting', 31, '105.53.220.72', 1446486609, 5, 17, '0', '0', '2015-11-02 17:50:09'),
(14, 'We deal with all kind of events.', 26, '197.232.15.156', 1446537042, 3, 19, '0', '0', '2015-11-03 07:50:42'),
(15, '10:21', 26, '41.139.223.198', 1446620806, 7, 31, '0', '0', '2015-11-04 07:06:46'),
(16, '10;24', 31, '41.139.223.198', 1446620977, 7, 26, '0', '0', '2015-11-04 07:09:37'),
(17, 'Hello asking where i can get tickets', 31, '41.139.223.198', 1446621606, 8, 28, '0', '0', '2015-11-04 07:20:06'),
(18, 'Hello at our biz centre on muindi mbingu street', 28, '41.139.223.198', 1446621758, 8, 31, '0', '0', '2015-11-04 07:22:38'),
(19, 'owk thankyou', 31, '41.139.223.198', 1446621981, 8, 28, '0', '0', '2015-11-04 07:26:21'),
(20, '10:41', 31, '41.139.223.198', 1446622040, 7, 26, '0', '0', '2015-11-04 07:27:20'),
(21, '10:41', 31, '41.139.223.198', 1446622046, 7, 26, '1', '0', '2015-11-04 07:27:26'),
(22, 'hello to', 31, '41.139.223.198', 1446622249, 4, 27, '1', '0', '2015-11-04 07:30:49'),
(23, 'hello to', 31, '41.139.223.198', 1446622249, 4, 27, '1', '0', '2015-11-04 07:30:49'),
(24, 'hello to', 31, '41.139.223.198', 1446622249, 4, 27, '0', '1', '2015-11-04 07:30:49'),
(25, 'hello to', 31, '41.139.223.198', 1446622253, 4, 27, '0', '1', '2015-11-04 07:30:53'),
(26, 'okay', 19, '197.232.15.156', 1446624218, 3, 26, '0', '0', '2015-11-04 08:03:38'),
(27, 'hey', 19, '197.232.15.156', 1446624385, 9, 28, '0', '0', '2015-11-04 08:06:26'),
(28, 'how are you doing', 26, '105.56.66.147', 1447306531, 7, 31, '0', '0', '2015-11-12 05:35:31'),
(29, 'message', 1, '::1', 1463730776, 10, 14, '0', '0', '2016-05-20 07:52:56'),
(30, 'First test', 33, '::1', 1464075853, 11, 40, '0', '0', '2016-05-24 07:44:13'),
(31, 'happy\r\n', 33, '::1', 1464077688, 12, 41, '0', '0', '2016-05-24 08:14:48'),
(32, 'crazy', 33, '::1', 1464077945, 11, 40, '1', '0', '2016-05-24 08:19:06');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `time_zone_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gallery_limit` int(11) NOT NULL DEFAULT '10',
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `role` enum('user','admin') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `sub_newsletters` tinyint(1) NOT NULL DEFAULT '0',
  `sub_sms_alerts` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_logged_in` datetime DEFAULT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` int(33) DEFAULT NULL,
  `ph_verified` tinyint(1) NOT NULL DEFAULT '0',
  `languages` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(2555) COLLATE utf8_unicode_ci NOT NULL,
  `time_zone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `time_zone_id`, `name`, `email`, `image_url`, `gallery_limit`, `password`, `role`, `active`, `sub_newsletters`, `sub_sms_alerts`, `remember_token`, `last_logged_in`, `video_url`, `phone`, `ph_verified`, `languages`, `location`, `time_zone`, `biography`, `created_at`, `updated_at`) VALUES
(7, 23, 'Faheem', 'naeemm@gmail.com', '/wb-content/img/faheem.jpg', 10, '$2y$10$IzrrG7x4nJPajNDMn3IkQO/tj61tqjuody9BpQxGIaTu2QQmTpZiC', 'user', 1, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, 'Lahore, Pakistan', NULL, NULL, '2015-11-26 17:45:26', '2015-11-26 17:45:26'),
(33, 92, 'Muhammad Naeem', 'mnaeemwahla@gmail.com', '/wb-content/img/MuhammadNaeem2-8t0H.jpg', 10, '$2y$10$KSG4ZCG1a/S3IKgIusqcV.zHkmszevf5p6JMxpP/bZ5bo/9wSbeKe', 'admin', 1, 1, 1, 'JPcddpQxx5nzDJqCvGtu1Ydv0DMH2FtRSZy9IdGjylkhgLJLhVIQhh4IiwHt', NULL, 'aaaaaaaaaaaaaaaaaaa', 300, 0, 'English', 'Lahore, Pakistan', NULL, 'Explore new places with ', '2015-12-06 10:18:30', '2016-04-13 10:35:24'),
(34, 7, 'waqar', 'w@gmail.com', '/wb-content/img/waqar-IRhN.jpg', 10, '$2y$10$KSG4ZCG1a/S3IKgIusqcV.zHkmszevf5p6JMxpP/bZ5bo/9wSbeKe', 'user', 1, 0, 0, 'Y5AVXAluZOXqTgQtu6sfR2PbtaLRQ4BDoreRyvuYZR1b3OUPldd7jdfKLD9T', NULL, '', NULL, 0, '', 'Lahore, Pakistan', NULL, '', '2015-12-07 04:42:14', '2016-04-13 11:09:10'),
(35, 66, 'Ali', 'ali@gmail.com', '/wb-content/img/waqar-IRhN.jpg', 10, '$2y$10$R1gx5BftE0TFbNN1mvlIFOlvThJ3ix4qwIpW88ooKDvPUMmVeli9y', 'user', 1, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, 'Lahore, Pakistan', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, NULL, 'naeem', 'test@gmail.com', '/wb-content/img/default-img.png', 10, '$2y$10$zwEWOr20Kvi.QIV/IiKGaux0t9/mbTvZL.OLuIs6IypzBuCzYOYOa', 'user', 1, 0, 0, 'b6juyWpTv4FtxgyjLqf33ugHtUgg4fVU0IxBZcGEX6Zvhi0L16vT6IIlM0ZO', NULL, NULL, NULL, 0, NULL, '', NULL, NULL, '2016-02-13 07:02:56', '2016-02-13 12:01:40'),
(37, NULL, 'test123', 'test@test.com', '/wb-content/img/default-img.png', 10, '$2y$10$PDnC32EqmMigCQcMre/.WOtH2xow.9UYVNxLYSM01y8c3uic79M6i', 'user', 1, 0, 0, 'sCHfiRQzD7UJCM3uwa2UMidp2ExeCGL1QqfsscTVovOxG2ilXegYaGqnlc7W', NULL, NULL, NULL, 0, NULL, '', NULL, NULL, '2016-03-13 04:18:16', '2016-03-14 02:32:27'),
(38, 17, 'naeem', 'n@gmail.com', '/wb-content/img/naeem-COvr.jpg', 10, '$2y$10$iIvPhgRNY2ig5kzAVRa88ObhkdzhLTl3VsQKF1nIeUu2HhKz0RFn2', 'user', 1, 1, 0, 'lOKpoq2BKpKsZNPFfS7dEeMYaKTU4T6AKIVlHjlgMrsHmvHXOjGMK42fOu74', NULL, '', 36333, 0, 'English', 'Lahore', NULL, 'Life is to live', '2016-03-27 16:38:50', '2016-03-27 17:06:43'),
(39, NULL, 'Admin1', 'admin@cityfy.com', '/wb-content/img/default-img.png', 10, '$2y$10$myJf4l0eKZx/GMEPPb7cAuqx0FacuucCvXHKwNekzGsaT3t/YRHXS', 'admin', 1, 0, 0, 'jXL3FSTCw1Cv3c3cBMZgZUQZ9KyRoC9pZHdgFrtqw4u1hL22OvlcHZe9puv8', NULL, NULL, NULL, 0, NULL, '', NULL, NULL, '2015-11-27 05:57:53', '2016-04-08 00:54:17'),
(40, 61, 'Andreas Klingenberger', 'andreas.klingenberger@gmail.com', '/wb-content/img/default-img.png', 10, '$2y$10$pPgS1.nvpspZSXiZipp7/e8uPjk3XC0Hl70Ci5nu/fcqZ//b1rgrG', 'user', 1, 1, 1, 'Mi0WHuXQp2CNw0FlqOAI1TDP0lxd7T937odCrvw3kvwEIn6yISXnFwC1S3EX', NULL, '', NULL, 0, 'Deutsch, Englisch', 'Erkelenz', NULL, 'ich bin ein passionierter Tourenguide und mag einfach gerne meine Heimatstadt Erkelenz. Diese Schöne Stadt möchte ich gerne anderen zeigen.', '2015-11-27 18:08:46', '2015-12-03 10:14:34'),
(41, 59, 'Michael Jaroya', 'liddypedia@gmail.com', '/wb-content/img/MichaelJaroya-YbVN.jpg', 10, '$2y$10$KSG4ZCG1a/S3IKgIusqcV.zHkmszevf5p6JMxpP/bZ5bo/9wSbeKe', 'user', 1, 0, 0, 'VMAXJ1kygOGhyCx2keoT24UnCNaT2FUrFd4wCxXC69ClmMto7atDuw7ST3IA', NULL, '', 1, 0, 'French', 'Paris', NULL, '', '2015-11-29 16:49:12', '2016-04-13 10:36:01'),
(42, NULL, 'admin', 'admin@gmail.com', NULL, 10, '21232f297a57a5a743894a0e4a801fc3', 'user', 1, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, '', NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_conversations`
--

CREATE TABLE `user_conversations` (
  `read_status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1:unread,0:reaad',
  `user_id` int(10) NOT NULL,
  `c_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_conversations`
--

INSERT INTO `user_conversations` (`read_status`, `user_id`, `c_id`) VALUES
('0', 1, 10),
('1', 14, 10),
('0', 17, 5),
('0', 19, 2),
('0', 19, 3),
('0', 19, 9),
('0', 26, 3),
('0', 26, 7),
('0', 27, 1),
('0', 27, 2),
('0', 27, 4),
('1', 28, 8),
('1', 28, 9),
('0', 31, 4),
('0', 31, 5),
('1', 31, 7),
('0', 31, 8),
('1', 32, 1),
('0', 33, 11),
('0', 33, 12),
('1', 40, 11),
('1', 41, 12);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `conversation`
--
ALTER TABLE `conversation`
  ADD PRIMARY KEY (`c_id`),
  ADD KEY `user_one` (`user_one`),
  ADD KEY `user_two` (`user_two`);

--
-- Indexes for table `conversation_reply`
--
ALTER TABLE `conversation_reply`
  ADD PRIMARY KEY (`cr_id`),
  ADD KEY `user_id_fk` (`user_id_fk`),
  ADD KEY `c_id_fk` (`c_id_fk`),
  ADD KEY `user_id_fk_2` (`user_id_fk_2`),
  ADD KEY `user_id_fk_2_2` (`user_id_fk_2`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_conversations`
--
ALTER TABLE `user_conversations`
  ADD PRIMARY KEY (`user_id`,`c_id`),
  ADD KEY `user_conversations_fk2` (`c_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `conversation`
--
ALTER TABLE `conversation`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `conversation_reply`
--
ALTER TABLE `conversation_reply`
  MODIFY `cr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
