<?php
$seller_id=33;
$category='Business';
?>
    <link rel="stylesheet" type="text/css" href="css/conversation.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <style type="text/css">
    .remove{display:none;}
    </style>
    <div class="row" style="margin-top:10px;">
        <div class="col-lg-3">
        <input type="hidden" value='33' id="user1" name="user1">
        <input type="hidden" value='' id="user2" name="user2">
        <input type="hidden" value='' id="c_id" name="c_id">
            <div class="btn-panel btn-panel-conversation">
                <a id="search_users" class="btn  col-lg-5 send-message-btn " role="button"><i class="fa fa-search"></i> Search</a>
                <a href="new_message.php" class="btn  col-lg-7  send-message-btn pull-right" role="button"><i class="fa fa-plus"></i> New Conversation</a>
            </div>
        </div>

        <div class="col-lg-offset-1 col-lg-7">
            <div class="btn-panel btn-panel-msg">
            </div>
        </div>

    </div>
  <div class="row form-group has-success has-feedback" id="search_id">
    <div class="col-sm-3">
      <input type="text" class="form-control " id="search_input" name="search_input" placeholder="Type username to filter">
    </div>

  </div>
    <div class="row">

        <div class="conversation-wrap col-lg-3">

            <?php require_once('fetch_conversations.php'); ?>
        </div>



        <div class="message-wrap col-lg-8">
            <div class="msg-wrap">
                <div class='media msg '>
                </div>
            </div>

            <div class="send-wrap ">

                <textarea id="real_message" class="form-control send-message" rows="3" placeholder="Write a reply..."></textarea>


            </div>
            <div class="btn-panel">
                <a class=" col-lg-4 text-right btn send-message-btn pull-right"  role="button" id="send" ><i class="fa fa-plus"></i> Send Message</a>
            </div>
        </div>
    </div>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/message.js"></script>
