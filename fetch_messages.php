<?php
$c_id=$_REQUEST['cid'];
$user=$_REQUEST['userid'];
require_once('config.php');
//the config file here
// the user id(preferably called through sessions)
//create db connection
$conn= new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
$qt3="UPDATE user_conversations SET read_status='0' WHERE user_id='$user' && c_id='$c_id'";
$rt3=mysqli_query($conn,$qt3);
//fetch all reply messages
$query="SELECT R.cr_id,R.time,R.reply,U.id,U.name,U.email,U.image_url 
FROM
users U, conversation_reply R 
WHERE R.user_id_fk=U.id
AND
R.c_id_fk='$c_id'
AND
CASE 
WHEN R.user_id_fk = '$user'
THEN R.sender_d_status ='0'
WHEN R.user_id_fk_2 = '$user'
THEN R.receiver_d_status='0'
END
ORDER BY R.cr_id ASC LIMIT 20";

$result=mysqli_query($conn,$query);
$the_date="";
echo "<div class='media msg '></div>";
while($row = mysqli_fetch_assoc($result))
{
$cr_id=$row['cr_id'];
$time=$row['time'];
$reply=$row['reply'];
$user_id=$row['id'];
$username=$row['name'];
$email=$row['email'];
$days_date=date("Y-m-d",$row['time']);
$today=date('Y-m-d');
//display messages
// echo $username.$reply."<br/>";

if ($the_date!=$days_date) {
   echo "<div class='alert alert-success msg-date'>
    <strong>";
    if($days_date!=$today)
      {
         echo(date("jS F Y",$row['time']));
      }
      else
      {
        echo "Today";
      }
    echo "</strong>
</div>";
}
echo "
                <div class='media msg '>
                    <a class='pull-left' href='#'>
                        <img class='media-object'  alt='user image' style='width: 32px; height: 32px;' src='../uploads/{$row['image_url']}'>
                    </a>
                    <div class='media-body'>
                        <small class='pull-right time'><i class='fa fa-clock-o'></i> ";
                        echo(date("g:i A",$row['time'])); 
                         echo "</small>";
                       
                       if($user==$user_id){
                       	echo "<h5 class='media-heading'>Me</h5>";
                       } 
                       else
                       {
                       echo "<h5 class='media-heading'>{$row['name']}</h5>";
                       }
                       echo "<a id='".$cr_id."' class='delete_message pull-right data-toggle='tooltip' data-placement='bottom' title='Delete''><span class='glyphicon glyphicon-trash'></span></a>";
                       echo" <small class='col-lg-10'>{$row['reply']}</small>
                    </div>
                </div>
";
$the_date=$days_date;
}
?>
