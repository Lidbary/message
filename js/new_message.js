$(document).ready(function(){
    $("#search_input").addClass('remove');
    $('#send').attr("disabled","disabled");
    $('#reply').keyup(function(){
        if($('#reply').val().length!=0)
        {
            $('#send').removeAttr("disabled");
         }
         else
         {
            $('#send').attr("disabled","disabled");
         }
    });
    $('body').on("click",".conv_id",function(){
    var element = $(this);
    var user_id = element.attr("id");
    var user2=$('#user2_'+user_id).val();
    var username=$('#md_'+user_id).text();
    $('#user2').val(user2);
    $('#keyword').val(username);
    $("#search_input").addClass("remove");
    $("#reply").focus();
});

 $('#search_users').click(function(){
    $("#search_input").toggleClass("remove");
    $("#search_input").focus();
}); 
var MIN_LENGTH = 1;
    $("#search_input").keyup(function() {
        $(this).attr('autocomplete', 'off');
        var search = $("#search_input").val();
        var user_id=$("#user1").val();
        var determiner=$("#determiner").val();
        if (search.length >= MIN_LENGTH) {
        $('.conversation-wrap').load("fetch_users.php",
        {
          userid:user_id,
          search:search,
          determiner:determiner
        }
        );
    }
    else
    {
       $('.conversation-wrap').load("fetch_users.php",
        {
          userid:user_id,
          determiner:determiner
        }
        );  
    }
    });

});
