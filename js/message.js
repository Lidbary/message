$(document).ready(function(){
var newsLoading = $('<img/>')
.attr({
'src': 'img/links.gif', 
'alt': 'loading. please wait', 
'width': '40px', 
'height': '40px'}
);

    $('#send').attr("disabled","disabled");
    $('#real_message').keyup(function(){
        if($('#real_message').val().length!=0)
        {
            $('#send').removeAttr("disabled");
         }
         else
         {
            $('#send').attr("disabled","disabled");
         }
    });

    setTimeout(function() {
        var user_id=$("#user1").val();
        $("#search_input").addClass('remove');
        $('.conversation-wrap').html(newsLoading);
        $('.conversation-wrap').load("fetch_conversations.php",
        {
          userid:user_id
        }
        );
    }, 0);
    setTimeout(function() {
        $(".conv_id").eq(0).click(); // tigger('click')
    }, 500);

    $('body').on("click",".conv_id",function(){
    var element = $(this);
    var conversation_id = element.attr("id");
    var user_id=$("#user1").val();
    var user2=$('#user2_'+conversation_id).val();
    $('#user2').val(user2);
    $('#c_id').val(conversation_id);
            $('.msg').delay(1000).fadeOut('slow',function(){
                $('.msg-wrap').html(newsLoading);
                $('.msg-wrap').load("fetch_messages.php",
              {
                  userid:user_id,
                  cid:conversation_id
              }
                 );
            });
    $('.conversation-wrap').load("fetch_conversations.php",
        {
          userid:user_id
        }
        );
    $("#search_input").addClass("remove");
    
    });

     $('body').on("click","#send",function(){
    var message=$('#real_message').val();
    var user1=$("#user1").val();
    var user2=$("#user2").val();
    var conversation_id=$('#c_id').val();
    var datasend = "reply="+message+"&user1="+ user1+"&user2="+user2;
    $.ajax({
      type:"POST",
      url:"compose.php",
      data:datasend,
      cache:false,
      success:function(msg){
      $('#real_message').val('');
      $('.msg').delay(1000).fadeOut('slow',function(){
            $('.msg-wrap').html(newsLoading);
            $('.msg-wrap').load("fetch_messages.php",
          {
              userid:user1,
              cid:conversation_id
          }
             );
            $('.conversation-wrap').html(newsLoading);
        $('.conversation-wrap').load("fetch_conversations.php",
        {
          userid:user1
        }
        );
        });
        $('#send').attr("disabled","disabled");
      }
    });
     });
 $('#search_users').click(function(){
    $("#search_input").toggleClass("remove");
    $("#search_input").focus();
}); 
var MIN_LENGTH = 1;
    $("#search_input").keyup(function() {
        $(this).attr('autocomplete', 'off');
        var search = $("#search_input").val();
        var user_id=$("#user1").val();
        if (search.length >= MIN_LENGTH) {
            // $('.conversation-wrap').html(newsLoading);
        $('.conversation-wrap').load("fetch_conversations.php",
        {
          userid:user_id,
          search:search
        }
        );
    }
    else
    {
        // $('.conversation-wrap').html(newsLoading);
        $('.conversation-wrap').load("fetch_conversations.php",
        {
          userid:user_id,
        }
        );   
    }
    });
$('body').on("click",".delete_message",function(){
    var element = $(this);
    var cr_id = element.attr("id");
    var user_id=$("#user1").val();
    var conversation_id=$('#c_id').val();
    var datasend = "crid="+cr_id+"&userid="+ user_id;

    $.ajax({
      type:"POST",
      url:"delete_message.php",
      data:datasend,
      cache:false,
      success:function(msg){
      $('#real_message').val('');
      $('.msg').delay(1000).fadeOut('slow',function(){
            $('.msg-wrap').html(newsLoading);
            $('.msg-wrap').load("fetch_messages.php",
          {
              userid:user_id,
              cid:conversation_id
          }
             );
            $('.conversation-wrap').html(newsLoading);
        $('.conversation-wrap').load("fetch_conversations.php",
        {
          userid:user_id
        }
        );
        });
        
      }
    });
});

});
