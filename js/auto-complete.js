var MIN_LENGTH = 2;

$( document ).ready(function() {
	$("#keyword").keyup(function() {
		$(this).attr('autocomplete', 'off');
		var keyword = $("#keyword").val();
		var user_one=$('#user1').val();
		var determiner=$("#determiner").val();
		if (keyword.length >= MIN_LENGTH) {
			
			$.get( "auto-complete.php", { keyword: keyword, user_one:user_one,determiner:determiner} )
			.done(function( data ) {
				$('#results').html('');
				var results = jQuery.parseJSON(data);
				$(results).each(function(key, value) {
					var username=this['name'];
					var email=this['email'];
					var user_id=this['id'];
					var logo_image=this['image_url'];
					$('#results').append('<div class="item" id="'+user_id+'"> <div class="media conversation"><a class="pull-left " ><img class="media-object"  alt="user image" style="width: 50px; height: 50px;" src="../uploads/'+logo_image+'"></a><div class="media-body"><h5 id="sear_'+user_id+'"class="media-heading">' + username + '</h5></div></div></div>');

				})

			    $('.item').click(function() {
			    	// var text = $(this).html();
			    	var userid=$(this).attr("id");
			    	var text=$("#sear_"+userid).text();
			    	$('#keyword').val(text);
			    	$('#user2').val(userid);
			    	$("#reply").focus();
			    });

			});
		} else {
			$('#results').html('');
		}
	});

    $("#keyword").blur(function(){
    		$("#results").fadeOut(500);
    	})
        .focus(function() {		
    	    $("#results").show();
    	});

});