<?php
require_once('config.php');
//the config file here
//create db connection
$conn= new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
// the user ids(preferably called through sessions)
$user_one=$_REQUEST['user1'];
$user_two=$_REQUEST['user2'];
$reply=$_REQUEST['reply'];
$reply=stripslashes($reply);
$reply = mysqli_real_escape_string($conn,$reply);
$reply=trim($reply);
//ensure there is no conversation created on a single user_id
if($user_one!=$user_two)
{
// check if the conversation between the two users exists
$q="SELECT c_id FROM conversation WHERE 
(user_one='$user_one' and user_two='$user_two') 
or (user_one='$user_two' and user_two='$user_one')";
$time=time();
$ip=$_SERVER['REMOTE_ADDR'];
$result=mysqli_query($conn,$q);
if(mysqli_num_rows($result)==0)
{
//create a new conversation
$query ="INSERT INTO conversation 
(user_one,user_two,ip,time) VALUES 
('$user_one','$user_two','$ip','$time')";
$result=mysqli_query($conn,$query);
// fetch the last created conversation id
$q="SELECT c_id FROM conversation 
WHERE user_one='$user_one' ORDER BY c_id DESC limit 1";
$result2=mysqli_query($conn,$q);
$v=mysqli_fetch_assoc($result2);
$cid=$v['c_id'];
$qt1="INSERT INTO user_conversations (user_id,c_id,read_status) VALUES ('$user_one','$cid','0')";
$qt2="INSERT INTO user_conversations (user_id,c_id,read_status) VALUES ('$user_two','$cid','1')";
$rt1=mysqli_query($conn,$qt1);
$rt2=mysqli_query($conn,$qt2);
}
else
{
// conversation exists so the id is fetched
$v=mysqli_fetch_assoc($result);
$cid=$v['c_id'];
$qt3="UPDATE user_conversations SET read_status='0' WHERE user_id='$user_one' && c_id='$cid'";
$qt4="UPDATE user_conversations SET read_status='1' WHERE user_id='$user_two' && c_id='$cid'";
$rt3=mysqli_query($conn,$qt3);
$rt4=mysqli_query($conn,$qt4);
$date_time=DATE('Y-m-d H:i:s');
$update_sql="UPDATE conversation SET ts='$date_time' WHERE c_id='$cid'";
$update_result=mysqli_query($conn,$update_sql);
}
$sql="INSERT INTO conversation_reply 
	(user_id_fk,user_id_fk_2,reply,ip,time,c_id_fk) VALUES 
	('$user_one','$user_two','$reply','$ip','$time','$cid')";
$result3=mysqli_query($conn,$sql);
}
if(isset($_REQUEST["send"])){
	header('location:message.php');
}
?>